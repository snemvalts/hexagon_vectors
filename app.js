var app = {};

app.canvasElement = document.getElementsByClassName("mainCanvas");

app.fabricCanvas = new fabric.Canvas("mainCanvas");

app.width = document.documentElement.clientWidth;
app.height = document.documentElement.clientHeight;

app.points = regularPolygonPoints(6,100);

function resizeCanvas(){
	app.fabricCanvas.setHeight(app.height);
	app.fabricCanvas.setWidth(app.width);
}
resizeCanvas();

function mergeArray(matrix, maxLen){
	if(typeof maxLen == "undefined") maxLen = matrix.length;
	var arr = [];
	for(var i = 0; i < matrix.length || i < maxLen; i++){
		arr.push(matrix[i].x,matrix[i].y);
	}
	return arr;
}

function getVector(sides){
	var vectorCoordinateX = sides[1][0] - sides[0][0];
	var vectorCoordinateY = sides[1][1] - sides[0][1];
	var vectorCoordinates = 
}

function getTwoRandomSides(){
	var arr = [];
	var baseRand = Math.floor(Math.random()*app.points.length-2);
	baseRand = baseRand < 0? 0 : baseRand;
	console.log(baseRand)

	arr.push([app.points[baseRand].x,app.points[baseRand].y,app.points[baseRand+1].x,app.points[baseRand+1].y]);
	arr.push([app.points[baseRand+1].x,app.points[baseRand+1].y,app.points[baseRand+2].x,app.points[baseRand+2].y]);

	return arr;
}

function getCentersForLines(coords){
	var arr = [];
	for(var i = 0; i < coords.length; i++){
		console.log(coords)
		var avgX = (coords[i][0]+coords[i][2])/2;
		var avgY = (coords[i][1]+coords[i][3])/2;
		arr.push([avgX,avgY]);
	}
	return arr;
}

function regularPolygonPoints(sideCount,radius){
    var sweep=Math.PI*2/sideCount;
    var cx=radius;
    var cy=radius;
    var points=[];
    for(var i=0; i<sideCount; i++){
        var x=cx+radius*Math.cos(i*sweep);
        var y=cy+radius*Math.sin(i*sweep);
        points.push({x:x,y:y});
    }
    console.log(points);
    return(points);
}

/*function centralizeLineCoordinates(coords){
	console.log("centr")
	for(var i = 0; i < coords.length; i++){
		//even, so y 
		if(i !== 0 && i % 2){
			coords[i] += Math.floor(app.height/2);
		}
		//odd
		else{
			coords[i] += Math.floor(app.width/2);
		}
	}
	return
}*/

function drawTwoLines(){
	//TODO: remove first two lines
	var sides = getTwoRandomSides();
	var center = getCentersForLines(sides);
	
	for(var i = 0; i < sides.length; i++){
		var line = new fabric.Line(sides[i],
			{	
				stroke:"red",
				originX:"center",
				originY:"center",
				strokeWidth:3
			}
		);
		 //console.log(center)
		line.setLeft(Math.floor(app.width/2)+center[i][0]-100);
		line.setTop(Math.floor(app.height/2)+center[i][1]-100);
		app.fabricCanvas.add(line);
	}
}

function initializePolygon(){
	var polygon = new fabric.Polygon(app.points, {
		originX:"center",
		originY:"center",
		hasBorders: false,
		hasControls: false,
		hasRotatingPoint: false,
		selectable:false,
    	stroke: 'black',
    	fill:undefined,
    	left: 50,
    	top: 50,
    	strokeWidth: 2,
    	strokeLineJoin: 'bevil'
	},false);
	polygon.setLeft(Math.floor(app.width/2));
	polygon.setTop(Math.floor(app.height/2));
	return polygon;
}

app.fabricCanvas.add(initializePolygon());
drawTwoLines();